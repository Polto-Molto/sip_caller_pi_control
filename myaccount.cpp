#include "myaccount.h"

MyAccount::MyAccount()
{
_pi = 0;
}

MyAccount::~MyAccount()
{

}

void MyAccount::setPiSetup(piSetup *p)
{
_pi = p;
}

void MyAccount::onRegStarted(OnRegStartedParam &prm)
{
	if(prm.renew)
	cout << " ************************************************* " << endl;
	else
	cout << " ################################################# " << endl;
}

void MyAccount::onRegState(OnRegStateParam &prm)
{
        cout << " ################################################# " << endl;
	cout << prm.code << endl;
        cout << " ################################################# " << endl;

    AccountInfo ai = getInfo();
    if(ai.regIsActive)
	call_enabled = true;
    else
	call_enabled = false;

    if(prm.code == 0)
        cout << " account Not register" << endl;
    if(prm.code == PJSIP_SC_FORBIDDEN)
        call_enabled = false;
    if(prm.code == PJSIP_SC_BAD_GATEWAY)
        cout << " account PJSIP_SC_BAD_GATEWAY" << endl;
    if(prm.code == PJSIP_SC_REQUEST_TIMEOUT)
	{
        cout << " account PJSIP_SC_REQUEST_TIMEOUT" << endl;
	call_enabled = false;
	}
    if(prm.code == PJSIP_SC_OK)
        cout << " account registerd ok " << endl;

}

void MyAccount::onIncomingCall(OnIncomingCallParam &iprm)
{
	MyCall *call = new MyCall(*this,iprm.callId);
	call->setPiSetup(_pi);
        CallOpParam prm;
        prm.statusCode = PJSIP_SC_OK;
        call->answer(prm);
}

bool MyAccount::callEnabled()
{
    return call_enabled;
}
