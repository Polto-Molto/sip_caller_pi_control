#include "gsmcontroller.h"

gsmController::gsmController(string d,int b,configFile *c)
		:_device(d),_baudrate(b),conf(c)
{
    register_failed_count = 0;
    connection_status = false;
    first_init = false;
    _pi = 0;
    gsm_users = 0;
    logfile = 0;
    incoming_call = false;
    mic_volume = 5;
    speaker_volume = 50;
    device_handle = serialOpen (_device.data(), _baudrate);
    if(device_handle > 0)
    {
//	connection_status = true;
   	reading = thread(&gsmController::send_thread ,this);
    	writing = thread(&gsmController::receive_thread ,this);
	reset();
    }
}

void gsmController::connectToPort()
{
    connection_status = false;
    device_handle = serialOpen (_device.data(), _baudrate);
    if(device_handle > 0)
    {
        connection_status = true;
        reading = thread(&gsmController::send_thread ,this);
        writing = thread(&gsmController::receive_thread ,this);
        reset();
    }
}

bool gsmController::connectionStatus()
{
   return connection_status;
}

void gsmController::setPiLink(piSetup* pi)
{
    _pi = pi;
}

void gsmController::setAllowedNumbersFile(gsmUsersFile* f)
{
  gsm_users = f;

  gsm_users->loadData();
}

void gsmController::setLoggerFile(logFile* f)
{
//  logfile = f;
}

void gsmController::reset()
{
  _registeration = false;
  full_function = false;
  call_enabled = false;
  sim_inside = false;
  netwrok_name ="";
  _signal = 0;
  last_dtmf = ' ';
  for(int i=0 ; i< KEYS ; i++)
    _key[i] = false;
}

void gsmController::send_thread()
{
  char buf[60];
  while(1)
				
  {
//    fgets(buf,60,stdin);
//    string str(buf);
//    serialPuts (device_handle, str.data());
  }
				
}

void gsmController::receive_thread()
{
  char buf[128];
  for(int i =0 ;i < 120 ;i++)
	buf[i] = '\0';
  int i = 0;
  unsigned int ch = 0;
  int time_start,time_end;
  time_start = time(NULL);
  int duration = 0;
  while(1)
  {
    if(first_init)
    {
       time_end = time(NULL);
       duration = time_end - time_start;
       if(duration > 60)
	{
	 connection_status = false;
	 break;
	}
    }

    while(( ch = serialGetchar(device_handle)) != '\r' )
    {
	if(first_init){
	   first_init = false;
	   connection_status = true;
	}

     	buf[i] = char(ch);
     	i++;
	if(i > 120)
	i = 0;
    }
    buf[i] ='\0';
    string data(buf);
    manipulateData(data);
    i=0;
  }
    string cmd = string("aplay ")+WAV_PTAH+VOICE_NO_GSM;
    system(cmd.data());
}

void gsmController::startRegisteration()
{
	
  serialPuts (device_handle, "ATE0\r");
  delay(500);
	
  serialPuts (device_handle, "AT+CRSL=0\r");
  //delay(500);
  delay(1000);
	
  serialPuts (device_handle, "AT+CREG=1\r");
  delay(500);
  request_Registeration();
}

void gsmController::setBaudrate(string b)
{
  string baud = string("AT+IPR=")+b+string("\r");
  serialPuts (device_handle,baud.data());
}

void gsmController::setMicVolumeLevel(string mv)
{
  mic_volume = mv; 
  string baud = string("AT+CMIC=0,")+mic_volume+string("\r");
  serialPuts (device_handle,baud.data());
}
void gsmController::setSpeakerVolumeLevel(string sv)
{
  speaker_volume = sv;
  string baud = string("AT+CLVL=")+ speaker_volume +string("\r");
  serialPuts (device_handle,baud.data());
}

void gsmController::setFunction()
{
  serialPuts (device_handle, "AT+CFUN=3\r");
}

void gsmController::activateDTMF()
{
  serialPuts (device_handle, "AT+DDET=1\r");
}

void gsmController::call(string num)
{
  string dial = string("ATD")+num+string(";\r");
  serialPuts (device_handle,dial.data());
}

void gsmController::hangUpCall()
{
  serialPuts (device_handle, "ATH\r");
}

void gsmController::answerCall()
{
  serialPuts (device_handle, "ATA\r");
}

bool gsmController::DTMFKey_status(int k)
{
  return _key[k];
}

int  gsmController::micLevel()
{
  return mic_level;
}

int  gsmController::signal()
{
  return _signal;
}

bool gsmController::registeration()
{
  return _registeration;
}

bool gsmController::callEnabled()
{
  return call_enabled;
}

void gsmController::request_Registeration()
{
  serialPuts (device_handle, "AT+CREG?\r");
}

void gsmController::request_DetectDTMF()
{
  serialPuts (device_handle, "AT+DDET?\r");
}

void gsmController::request_ModuleFunction()
{
  serialPuts (device_handle, "AT+CFUN?\r");
}

void gsmController::request_CallEnable()
{
  serialPuts (device_handle, "AT+CCALR?\r");
}

void gsmController::request_SIMEnable()
{
  serialPuts (device_handle, "AT+CPIN?\r");
}

void gsmController::request_Signal()
{
  serialPuts (device_handle, "AT+CSQ?\r");
}

void gsmController::request_CallLineID()
{
  serialPuts (device_handle, "AT+CLIP?\r");
}

void gsmController::request_MuteControl()
{
  serialPuts (device_handle, "AT+CMUT?\r");
}

void gsmController::request_MicLevel()
{
  serialPuts (device_handle, "AT+CMIC?\r");
}

void gsmController::request_ListCurrentControl()
{
  serialPuts (device_handle, "AT+CLCC\r");
}

void gsmController::request_DTMFToneGen()
{
  serialPuts (device_handle, "AT+VTS?\r");
}

void gsmController::request_DTMFToneDuration()
{
  serialPuts (device_handle, "AT+VTD?\r");
}

void gsmController::request_MobileOriginatedCall()
{
  serialPuts (device_handle, "AT+MORING?\r");
}

void gsmController::request_ModuleBaudRate()
{
  serialPuts (device_handle, "AT+IPR?\r");
}

bool gsmController::manipulateData(string data)
{
  cout << data << endl;
  int i = 0;

  i = data.find("+CREG:");
  if(i > 0)
  {
    response_Registeration(data);
    return true;
  }
  
  i = data.find("+DTMF:");
  if(i > 0)
  {
    response_DetectDTMF(data);
    return true;
  }

  i = data.find("+CFUN:");
  if(i > 0)
  {
    response_ModuleFunction(data);
    return true;
  }

  i = data.find("+CCALR:");
  if(i > 0)
  {
    response_CallEnable(data);
    return true;
  }

  i = data.find("+CPIN");
  if(i > 0)
  {
    response_SIMEnable(data);
    return true;
  }

  i = data.find("+CSQ");
  if(i > 0)
  {
    response_Signal(data);
    return true;
  }

  i = data.find("+CLIP:");
  if(i > 0)
  {
    response_CallLineID(data);
    return true;
  }

  i = data.find("+CMUT:");
  if(i > 0)
  {
    response_MuteControl(data);
    return true;
  }

  i = data.find("+CMIC:");
  if(i > 0)
  {
    response_MicLevel(data);
    return true;
  }

  i = data.find("+CLCC:");
  if(i > 0)
  {
    response_ListCurrentControl(data);
    return true;
  }

  i = data.find("+VTS:");
  if(i > 0)
  {
    response_DTMFToneGen(data);
    return true;
  }

  i = data.find("+VTD:");
  if(i > 0)
  {
    response_DTMFToneDuration(data);
    return true;
  }

  i = data.find("+MORING:");
  if(i > 0)
  {
    response_MobileOriginatedCall(data);
    return true;
  }

  i = data.find("+IPR:");
  if(i > 0)
  {
    response_ModuleBaudRate(data);
    return true;
  }

  i = data.find("NO CARRIER");
  if(i > 0 ) 
  {
        _pi->setPowerSwitchActivation(true);
  }

  i = data.find("NO DIALTONE");
  if(i > 0 ) 
  {
        _pi->setPowerSwitchActivation(true);
  }

  i = data.find("RING");
  if(i > 0 ) 
  {
    response_Incoming_Call(data);
  }

  i = data.find("BUSY");
  if(i > 0 ) 
  {
        _pi->setPowerSwitchActivation(true);
  }

  if(!module_started)
  {
  i = data.find("OK");
  if(i > 0 )
  {
    module_started = true;
    startRegisteration();
  }
  else
     serialPuts (device_handle, "AT\r");
  }


 return false;
}

void gsmController::response_Registeration(string data)
{
  int i = data.find("1,1");
  if(i < 0)
  {
	cout << "Start Registeration ..." << data << endl;
        if(_registeration)
	{
          _registeration = false;
	   string cmd = string("aplay ")+WAV_PTAH+VOICE_NO_GSM;
            delay(10000);
           system(cmd.data());
	}
	if(register_failed_count >= 5)
	{
	    _pi->switchGsmModuleOn();
	    register_failed_count = 0;
	}
	else
	{
         register_failed_count++;
	 startRegisteration();
  	 request_Registeration();
	}
        delay(10000);
  }
  else
  {
        if(!_registeration)
	{
          register_failed_count = 0;
          cout << " Netwrok Registered ..." << data << endl;
          serialPuts (device_handle, "AT+CLIP=1\r");
          request_CallEnable();
	  _registeration = true;
	  string cmd = string("aplay ")+WAV_PTAH+VOICE_GSM_SWITCH_ON;
          delay(10000);
          system(cmd.data());
//          delay(10000);
	}
  }
}

void gsmController::response_ModuleFunction(string data)
{

}

void gsmController::response_CallEnable(string data)
{
  int i = data.find("1");
  if(i < 0)
  {
    cout << "Call Not Enabled ..." << data << endl;
    call_enabled = false;
    string cmd = string("aplay ")+WAV_PTAH+VOICE_CHECK_SIM;
    system(cmd.data());
  }
  else
  {
    cout << " Call Enabled ..." << data << endl;
    call_enabled = true;
  }
}

void gsmController::response_SIMEnable(string data)
{

}

void gsmController::response_Signal(string data)
{

}

void gsmController::response_CallLineID(string data)
{
  if(incoming_call)
  {
    incoming_call = false;
    vector<string> v = explode(data, '"');
    string number = v[1];
    if(number.at(0) == '+');
	number.erase(0,1);
    string user_name = gsm_users->isUserInList(number);
    cout << "check : " << user_name << "::" << conf->gsmCallcenter_1() << endl;
    cout << "check : " << user_name << "::" << conf->gsmCallcenter_2() << endl;
    if(user_name.size() > 0)
    {
      cout <<"Phone Number : " << number << endl;
      cout << "user : " << user_name << endl;
      cout << " Request Open The Gate" << endl;
//      answerCall();
                  digitalWrite (DOOR_LOCK, HIGH);
                  delay(DOOR_OPEN_INTERVAL);
                  digitalWrite (DOOR_LOCK, LOW);

      hangUpCall();
      time_t t = time(0);   // get time now
      struct tm * now = localtime( & t );
      string call_date = to_string(now->tm_year + 1900)
                         + string("-") + to_string(now->tm_mon + 1)
                         + string("-") + to_string(now->tm_mday);

      string call_time = to_string(now->tm_hour)
                         + string(":") + to_string(now->tm_min)
                         + string(":") + to_string(now->tm_sec);

      string operation = "open by";

    logfile = new logFile(LOG_PTAH+FILE_GSM_LOG);
    if(!logfile->isOpened())
        cout << " cannot open log file ... " << endl;
    else
	logfile->addLine(call_date , call_time , operation ,user_name , number);
    delete logfile;
    logfile=0;
    }
   else if(number == conf->gsmCallcenter_1()|| number == conf->gsmCallcenter_2() )
   {
   time_t t = time(0);   // get time now
      struct tm * now = localtime( & t );
      string call_date = to_string(now->tm_year + 1900)
                         + string("-") + to_string(now->tm_mon + 1)
                         + string("-") + to_string(now->tm_mday);

      string call_time = to_string(now->tm_hour)
                         + string(":") + to_string(now->tm_min)
                         + string(":") + to_string(now->tm_sec);

      string operation = "open by";

   logfile = new logFile(LOG_PTAH+FILE_GSM_LOG);
    if(!logfile->isOpened())
        cout << " cannot open log file ... " << endl;
    else
        logfile->addLine(call_date , call_time , operation ,"call center" , number);
    delete logfile;
    logfile=0;
      cout << "Ringing Number : " << number << " Is Not In The User List ... " << endl;
   }
    else
    {
      cout << "Ringing Number : " << number << " Is Not In The User List ... " << endl;
      hangUpCall();
    }
  }
}

void gsmController::response_MuteControl(string data)
{

}

void gsmController::response_MicLevel(string data)
{

}

void gsmController::response_ListCurrentControl(string data)
{

}

void gsmController::response_DTMFToneGen(string data)
{

}

void gsmController::response_DTMFToneDuration(string data)
{

}

void gsmController::response_MobileOriginatedCall(string data)
{

}

void gsmController::response_ModuleBaudRate(string data)
{

}

void gsmController::response_Incoming_Call(string data)
{
   incoming_call = true;
}

void gsmController::response_DetectDTMF(string data)
{
    int i = data.find("+DTMF:");
    char dtmf = data.substr(i+7,1)[0];
    switch(dtmf)
    {
	    case '1':
		if(_key[KEY_1])
			_key[KEY_1] = false;
		else
			_key[KEY_1] = true;
		break;

            case '2':
                if(_key[KEY_2])
                        _key[KEY_2] = false;
                else
                        _key[KEY_2] = true;
                break;

            case '3':
                if(_key[KEY_3])
                        _key[KEY_3] = false;
                else
                        _key[KEY_3] = true;
                  digitalWrite (DOOR_LOCK, HIGH);
                  delay(DOOR_OPEN_INTERVAL);
                  digitalWrite (DOOR_LOCK, LOW);
		break;

            case '4':
                if(_key[KEY_4])
                        _key[KEY_4] = false;
                else
                        _key[KEY_4] = true;
                break;

            case '5':
                if(_key[KEY_5])
                        _key[KEY_5] = false;
                else
                        _key[KEY_5] = true;
                break;

            case '6':
                if(_key[KEY_6])
                        _key[KEY_6] = false;
                else
                        _key[KEY_6] = true;
                break;

            case '7':
                if(_key[KEY_7])
                        _key[KEY_7] = false;
                else
                        _key[KEY_7] = true;
                break;

            case '8':
                if(_key[KEY_8])
                        _key[KEY_8] = false;
                else
                        _key[KEY_8] = true;
                break;

            case '9':
                if(_key[KEY_9])
                        _key[KEY_9] = false;
                else
                        _key[9] = true;
                break;

            case '0':
                if(_key[KEY_0])
                        _key[KEY_0] = false;
                else
                        _key[KEY_0] = true;
                break;

            case '*':
                if(_key[KEY_STAR])
                        _key[KEY_STAR] = false;
                else
                        _key[KEY_STAR] = true;
                break;

            case '#':
                if(_key[KEY_BLOCK])
                        _key[KEY_BLOCK] = false;
                else
                        _key[KEY_BLOCK] = true;
                break;
    }
 }

vector<string> gsmController::explode(string s, char delim)
{
 vector<string> result;
  stringstream ss(s); // Turn the string into a stream.
  string tok;

  while(getline(ss, tok, delim)) {
    result.push_back(tok);
  }
    return result;
}

