MAKEFILE      = Makefile

####### Compiler, tools and options

CC            = gcc
CXX           = g++
CFLAGS        = -pipe -O2 -Wall -W -D_REENTRANT -fPIC $(DEFINES)
CXXFLAGS      = -std=c++11 -pipe -O2 -Wall -W -D_REENTRANT -fPIC $(DEFINES)
INCPATH       = -I. -I/usr/local/include -I. 
DEL_FILE      = rm -f
CHK_DIR_EXISTS= test -d
MKDIR         = mkdir -p
COPY          = cp -f
COPY_FILE     = cp -f
COPY_DIR      = cp -f -R
INSTALL_FILE  = install -m 644 -p
INSTALL_PROGRAM = install -m 755 -p
INSTALL_DIR   = cp -f -R
DEL_FILE      = rm -f
SYMLINK       = ln -f -s
DEL_DIR       = rmdir
MOVE          = mv -f
PKG_CONFIG_PATH = /usr/local/lib/pkgconfig
TAR           = tar -cf
COMPRESS      = gzip -9f
DISTNAME      = sip_caller.1.0.0
DISTDIR = 
LINK          = g++
LFLAGS        = -Wl,-O1 -Wl,-z,origin -Wl,-rpath,\$$ORIGIN
SUBLIBS	      = -L/usr/local/lib
LIBS          = $(SUBLIBS)  -lwiringPi -lpthread \
										/usr/local/lib/libpjlib-util.so \
										/usr/local/lib/libpjmedia-audiodev.so \
										/usr/local/lib/libpjmedia-codec.so\
										/usr/local/lib/libpjmedia.so\
										/usr/local/lib/libpjmedia-videodev.so\
										/usr/local/lib/libpjnath.so\
										/usr/local/lib/libpjsip-simple.so\
										/usr/local/lib/libpjsip.so\
										/usr/local/lib/libpjsip-ua.so\
										/usr/local/lib/libpj.so\
										/usr/local/lib/libpjsua2.so \
										/usr/local/lib/libpjsua.so
AR            = ar cqs
RANLIB        = 
SED           = sed
STRIP         = strip

####### Output directory

OBJECTS_DIR   = ./

####### Files
SOURCES       = main.cpp gsmcontroller.cpp sipcall.cpp myaccount.cpp mycall.cpp pisetup.cpp logfile.cpp gsmusersfile.cpp configfile.cpp ini.c INIReader.cpp
OBJECTS       = main.o   gsmcontroller.o   sipcall.o   myaccount.o   mycall.o   pisetup.o   logfile.o   gsmusersfile.o   configfile.o   ini.o INIReader.o
DIST          = define.h main.cpp
QMAKE_TARGET  = caller
DESTDIR       = 
TARGET        = caller


first: all
####### Build rules

$(TARGET):  $(OBJECTS)  
	$(LINK) $(LFLAGS) -o $(TARGET) $(OBJECTS) $(OBJCOMP) $(LIBS)


all: Makefile $(TARGET)

dist: distdir FORCE
	(cd `dirname $(DISTDIR)` && $(TAR) $(DISTNAME).tar $(DISTNAME) && $(COMPRESS) $(DISTNAME).tar) && $(MOVE) `dirname $(DISTDIR)`/$(DISTNAME).tar.gz . && $(DEL_FILE) -r $(DISTDIR)

distdir: FORCE
	@test -d $(DISTDIR) || mkdir -p $(DISTDIR)
	$(COPY_FILE) --parents $(DIST) $(DISTDIR)/
	$(COPY_FILE) --parents main.cpp $(DISTDIR)/


clean: FORCE 
	-$(DEL_FILE) $(OBJECTS)
	-$(DEL_FILE) $(TARGET)


distclean: clean 
	-$(DEL_FILE) $(TARGET) 
	-$(DEL_FILE) Makefile

####### Compile
ini.o: ini.c ini.h
INIReader.o: INIReader.cpp INIReader.h
logfile.o: logfile.cpp logfile.h
gsmusersfile.o: gsmusersfile.cpp gsmusersfile.h
configfile.o: configfile.cpp configfile.h
pisetup.o: pisetup.cpp pisetup.h
mycall.o: mycall.cpp mycall.h
myaccount.o: myaccount.cpp myaccount.h
sipcall.o: sipcall.cpp sipcall.h
gsmcontroller.o: gsmcontroller.cpp gsmcontroller.h
main.o: main.cpp
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o main.o main.cpp

####### Install

install:  FORCE

uninstall:  FORCE

FORCE:

