#ifndef PISETUP
#define PISETUP

#include "defines.h"

class piSetup
{
	public:
		piSetup();
		void init();
		void setPowerSwitchPushed(bool);
		void setPowerSwitchActivation(bool);

		void switchGsmModuleOn();

		bool powerSwitchPushed();
		bool powerSwitchActivation();
		void setHeartUp();
		void setHeartDown();
		void setIPCallOn();
		void setIPCallOff();

		void setSipCallAnswered(bool);
		bool sipCallAnswered();

		void setGsmCallAnswered(bool);
		bool gsmCallAnswered();

		void setSipCallStart(bool);
		bool isSipCallStart();

        void switchToGsm(bool);
        bool isSwitchToGsm();

	private:
		bool power_switch_pushed;
		bool power_switch_activation;
		bool sip_call_answered;
		bool gsm_call_answered;
		bool sip_call_start;
		bool gsm_call_start;
		bool switch_to_gsm;
};
#endif
