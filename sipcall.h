#ifndef SIPCALL
#define SIPCALL
#include "defines.h"
#include "myaccount.h"
#include "pisetup.h"
#include "logfile.h"

class sipCall
{
public:
	sipCall(piSetup*,string,string,string,bool*);
	~sipCall();
	void account_register();
	void startCall(string);
	void hangup();

	bool callEnabled();

private:
        bool* connection_flag;
	piSetup * _pi;
	Endpoint ep;
 	EpConfig ep_cfg;
  	TransportConfig tcfg;
	MyAccount *acc;
	MyCall *call;

	string _host;
	string _name;
	string _pass;
	string _caller;
};
#endif
