#include "defines.h"
#include "mycall.h"

MyCall::MyCall(Account &acc, int call_id)
: Call(acc, call_id)
{
_pi = 0;
logfile = 0; 
callAnswered= false;
}

MyCall::~MyCall()
{
delete logfile;
logfile =0;
//delete threadConnection; 
}

void MyCall::setPiSetup(piSetup *p)
{
_pi=p;
}

bool MyCall::isCallAnswered()
{
  return callAnswered;
}

/*
void MyCall::onCreateMediaTransportSrtp(OnCreateMediaTransportSrtpParam &prm)
{
cout << " onCreateMediaTransportSrtp emitted .... " << endl;
}
*/
void MyCall::onCreateMediaTransport(OnCreateMediaTransportParam &prm)
{
cout << "onCreateMediaTransport emitted .... " << endl;
}
void MyCall::onCallMediaEvent(OnCallMediaEventParam &prm)
{
cout << "onCallMediaEvent emitted .... " << endl;
}
void MyCall::onCallTransferStatus(OnCallTransferStatusParam &prm)
{
cout << "onCallTransferStatus emitted .... " << endl;
}
void MyCall::onStreamDestroyed(OnStreamDestroyedParam &prm)
{
cout << "onStreamDestroyed emitted .... " << endl;
}

// Notification when call's state has changed.
void MyCall::onCallState(OnCallStateParam &prm)
{
   cout << "OnCallStateParam Emitted ...." <<endl;
    CallInfo ci = getInfo();

    if (ci.state ==  PJSIP_INV_STATE_CONFIRMED) {
      callAnswered = true;
        }
    if (ci.state == PJSIP_INV_STATE_DISCONNECTED) {
        cout << "*************************************************************************" <<endl;
        cout << "*************************************************************************" <<endl;
        cout << "************************ CALL ENDED .************************************" <<endl;
        cout << "*************************************************************************" <<endl;
        cout << "*************************************************************************" <<endl;

        _pi->setIPCallOff();
        _pi->setPowerSwitchActivation(true);
        _pi->setSipCallAnswered(false);
	cout << " call ended " << endl;
        delete this;
    }
}

void MyCall::onCallMediaTransportState(OnCallMediaTransportStateParam &prm)
{
   cout << "onCallMediaTransportState Emitted ...." <<endl;

	if(prm.state == PJSUA_MED_TP_DISABLED)
        {
        cout << "*************************************************************************" <<endl;
        cout << "*************************************************************************" <<endl;
        cout << "************************ CALL ENDED .************************************" <<endl;
        cout << "*************************************************************************" <<endl;
        cout << "*************************************************************************" <<endl;

        _pi->setIPCallOff();
        _pi->setPowerSwitchActivation(true);
        _pi->setSipCallAnswered(false);
        cout << " call ended " << endl;
        delete this;
	}
}

// Notification when call's media state has changed.
void MyCall::onCallMediaState(OnCallMediaStateParam &prm)
{
   cout << "onCallMediaState Emitted ...." <<endl;

    CallInfo ci = getInfo();

    for (unsigned i = 0; i < ci.media.size(); i++) {
        if (ci.media[i].type==PJMEDIA_TYPE_AUDIO && getMedia(i)) {
            AudioMedia *aud_med = (AudioMedia *)getMedia(i);

            // Connect the call audio media to sound device
            AudDevManager& mgr = Endpoint::instance().audDevManager();
            aud_med->startTransmit(mgr.getPlaybackDevMedia());
            mgr.getCaptureDevMedia().startTransmit(*aud_med);
	    //threadConnection = new thread(&MyCall::testConnection,this);
        }
    }
}

void MyCall::onDtmfDigit(OnDtmfDigitParam &prm)
{
	int key_num = stoi(prm.digit) ;

  if(key_num  == DOOR_LOCK)
  {
    cout << "Gate Opened" << endl;
    digitalWrite (DOOR_LOCK, HIGH);
    delay(DOOR_OPEN_INTERVAL);
    digitalWrite (DOOR_LOCK, LOW);
      time_t t = time(0); // get time now
      struct tm * now = localtime( & t );
      string call_date = to_string(now->tm_year + 1900)
                         + string("-") + to_string(now->tm_mon + 1)
                         + string("-") + to_string(now->tm_mday);
     string call_time = to_string(now->tm_hour)
                         + string(":") + to_string(now->tm_min)
                         + string(":") + to_string(now->tm_sec);
       string operation = "open by";

	logfile = new logFile(LOG_PTAH+FILE_IP_LOG);
	if(!logfile->isOpened())
  	    cout << "cannot open log file ..." << endl;
	else
	    logfile->addLine(call_date , call_time , operation ,"IP Call Center" , "IP Call Center"); 
        delete logfile;
        logfile=0;

   }
}

void MyCall::onCallTsxState(OnCallTsxStateParam &prm)
{

cout << "onCallTsxState emitted ...... " << endl;
  /*  "application/dtmf-relay" */
  string str = prm.e.body.tsxState.src.rdata.wholeMsg;
  cout << "----------------------------------------------------"<<endl;
  cout << "----------------------------------------------------"<<endl;
  cout << "type: " << prm.e.type << endl;
  cout << "tx msg: " << prm.e.body.txMsg.tdata.wholeMsg << endl;
  cout << "error msg: " << prm.e.body.txError.tdata.wholeMsg << endl;
  cout << "error msg tsx: " << prm.e.body.txError.tsx.state << endl;
  cout << "error msg tsx: " << prm.e.body.txError.tsx.statusText << endl;
  cout << "rx msg : " << prm.e.body.rxMsg.rdata.wholeMsg << endl;
  cout << "----------------------------------------------------"<<endl;
  cout << "----------------------------------------------------"<<endl;
  int i = 0; 
  int key_num = -1;
  i = str.find("dtmf-relay");
  if(i > 0)
  {
    i = str.find("Signal=");
    key_num = std::stoi(str.substr(i+7,1));
    if(key_num  == DOOR_LOCK)
    {
      cout << "Gate Opened" << endl;
      digitalWrite (DOOR_LOCK, HIGH);
      delay(DOOR_OPEN_INTERVAL);
      digitalWrite (DOOR_LOCK, LOW);

      time_t t = time(0); // get time now
      struct tm * now = localtime( & t );
      string call_date = to_string(now->tm_year + 1900)
                         + string("-") + to_string(now->tm_mon + 1)
                         + string("-") + to_string(now->tm_mday);
      string call_time = to_string(now->tm_hour)
                         + string(":") + to_string(now->tm_min)
                         + string(":") + to_string(now->tm_sec);
      string operation = "open by";
      
	logfile = new logFile(LOG_PTAH+FILE_IP_LOG);
	if(!logfile->isOpened())
	  cout << "cannot open log file ..." << endl;
	else
          logfile->addLine(call_date , call_time , operation ,"IP Call Center" ,"IP Call Center");
	delete logfile;
	logfile=0;
    }
  }
}
