#include "logfile.h"

logFile::logFile(string n)
	:_name(n)
{
	_file.open(n,ios::app);
  if (!_file.is_open())
  {
  		_file.open(n, ios::out);
		if(_file.is_open())
		{
		file_opened = true;
  		_file << "Date\tTime\tOperation\tUser Name\tTel. Number" << endl;
		}
		else
		file_opened = false;
  }
  else
    file_opened = true;

}

logFile::~logFile()
{
   _file.close();
}

void logFile::close()
{
   _file.close();
}

bool logFile::isOpened()
{
	return file_opened;
}

void logFile::addLine(string d,string t,string o,string u,string n)
{
	string sp(" ");
	string dataLine  = d+sp+t+sp+o+sp+u+sp+n;
	_file << dataLine << endl;
}
