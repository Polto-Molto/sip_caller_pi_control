#ifndef MAIN_DEF
#define MAIN_DEF

#include <pjsua2.hpp>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <memory>
#include <wiringPi.h>
#include <wiringSerial.h>
#include <thread>         // std::thread
#include <vector>
#include <sstream>
#include <ctime>

using namespace pj;
using namespace std;

#define PJ_IS_LITTLE_ENDIAN 1
#define PJ_IS_BIG_ENDIAN 0

#define IDLE 0
#define SIP_CALL 1
#define GSM_CALL 2

#define POWER_SWITCH 23
#define DOOR_LOCK 3
#define DOOR_OPEN_INTERVAL 3000

#define GSM_POWER_KEY 5
#define GSM_EXTERNAL_POWER_RELAY 4
#define GSM_BASE_MAX_LINE_LENGTH  128

#define HEART_PIN  29
#define IP_CALL_LED  27

const string CONFIG_PTAH = "/home/pi/sip_caller_pi_control/config_files/";
const string LOG_PTAH = "/home/pi/logs/";
const string FILE_MAIN_INFO = "reg_info.txt";
const string FILE_GSM_LOG = "gsm_log.txt";
const string FILE_GSM_BASE = "gsm_BASE.csv";
const string FILE_IP_LOG = "ip_log.txt";

const string WAV_PTAH = "/home/pi/sip_caller_pi_control/wav/";
const string VOICE_CHECK_SIM = "Check_SIM_CARD.wav";
const string VOICE_GSM_SWITCH_ON = "GSM_Switch_ON.wav";
const string VOICE_NO_GSM = "NO_GSM.wav";
const string VOICE_NO_INTERNET = "NO_Internet.wav";
const string VOICE_OS_LOADED = "OS_loaded.wav";
const string VOICE_INTERNET_PRESENT = "Internet_present.wav";
const string VOICE_WAIT_RESPONSE  = "wait_response.wav";

#endif

