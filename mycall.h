#include "defines.h"
#include "logfile.h"
#include "pisetup.h"

class MyCall : public Call
{
public:
  MyCall(Account &acc, int call_id = PJSUA_INVALID_ID);
  ~MyCall();

  bool isCallAnswered();
  void setPiSetup(piSetup*);
  // Notification when call's state has changed.
  virtual void onCallState(OnCallStateParam &prm);

  virtual void onCallMediaTransportState(OnCallMediaTransportStateParam &prm);

//virtual void onCreateMediaTransportSrtp(OnCreateMediaTransportSrtpParam &prm);
 virtual void onCreateMediaTransport(OnCreateMediaTransportParam &prm);
virtual void onCallMediaEvent(OnCallMediaEventParam &prm);
virtual void onCallTransferStatus(OnCallTransferStatusParam &prm);
 virtual void onStreamDestroyed(OnStreamDestroyedParam &prm);

  // Notification when call's media state has changed.
  virtual void onCallMediaState(OnCallMediaStateParam &prm);

  virtual void onDtmfDigit(OnDtmfDigitParam &prm);
  virtual void onCallTsxState(OnCallTsxStateParam &prm);
  
private:
bool callAnswered;
piSetup *_pi;
logFile *logfile;
};
