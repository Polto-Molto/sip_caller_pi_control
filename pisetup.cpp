#include "pisetup.h"

piSetup::piSetup()
{
    power_switch_pushed = false;
    power_switch_activation = true;
        switch_to_gsm = false;

}

void piSetup::init()
{
    wiringPiSetup ();
//    wiringPiSetupGpio();
    pinMode (DOOR_LOCK,OUTPUT);
    pinMode (GSM_EXTERNAL_POWER_RELAY,OUTPUT);
    pinMode (GSM_POWER_KEY,OUTPUT);
    pinMode (HEART_PIN,OUTPUT);
    pinMode (IP_CALL_LED,OUTPUT);
 
    digitalWrite (DOOR_LOCK,LOW);
    digitalWrite (GSM_EXTERNAL_POWER_RELAY,LOW);
    digitalWrite (GSM_POWER_KEY,LOW);
    digitalWrite (IP_CALL_LED,HIGH);

    pinMode (POWER_SWITCH,INPUT);
}

void piSetup::switchGsmModuleOn()
{
    digitalWrite(GSM_EXTERNAL_POWER_RELAY,LOW);
    delay(10000);
    digitalWrite(GSM_EXTERNAL_POWER_RELAY,HIGH);
    delay(500);
    digitalWrite(GSM_POWER_KEY,LOW);
    delay(2000);
    digitalWrite(GSM_POWER_KEY,HIGH);
    delay(10000);
    cout << " module start up .. " << endl;
//    digitalWrite(GSM_POWER_KEY,HIGH);
}

void piSetup::setHeartUp()
{
   digitalWrite(HEART_PIN,HIGH);
}

void piSetup::setHeartDown()
{
  digitalWrite(HEART_PIN,LOW);
}

void piSetup::setIPCallOn()
{
  digitalWrite(IP_CALL_LED,LOW);
}

void piSetup::setIPCallOff()
{
  digitalWrite(IP_CALL_LED,HIGH);
}


void piSetup::setPowerSwitchPushed(bool state)
{
	power_switch_pushed = state;
}

bool piSetup::powerSwitchPushed()
{
	return power_switch_pushed;
}


void piSetup::setPowerSwitchActivation(bool state)
{
        power_switch_activation = state;
}

bool piSetup::powerSwitchActivation()
{
        return power_switch_activation;
}


void piSetup::setSipCallAnswered(bool v)
{
    sip_call_answered = v;
}

bool piSetup::sipCallAnswered()
{
    return sip_call_answered;
}

void piSetup::setGsmCallAnswered(bool v)
{
    gsm_call_answered = v;
}

bool piSetup::gsmCallAnswered()
{
    return gsm_call_answered;
}

void piSetup::setSipCallStart(bool v)
{
    sip_call_start = v;
}

bool piSetup::isSipCallStart()
{
    return sip_call_start;
}

void piSetup::switchToGsm(bool v)
{
    switch_to_gsm = v;
}

bool piSetup::isSwitchToGsm()
{
    return switch_to_gsm ;
}
