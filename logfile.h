#ifndef LOG_FILE
#define LOG_FILE
#include "defines.h"

class logFile
{
public:
	logFile(string);
	~logFile();
	bool isOpened();
	void addLine(string,string,string,string,string);
	void close();

private:
	string _name;
	fstream _file;
	bool file_opened;
};

#endif
