#include "configfile.h"

configFile::configFile(string n)
	:_name(n)
{
	  reader = new INIReader(_name);

    if (reader->ParseError() < 0)
        std::cout << "Can't load 'test.ini'\n";
}

int configFile::openFile()
{
	return 0;
}

int configFile::loadData()
{
	return 0;
}

string configFile::sipHost()
{	
	return reader->Get("config", "sip_host", "UNKNOWN");
}

string configFile::sipUser()
{
	return reader->Get("config", "sip_user", "UNKNOWN");
}

string configFile::sipPass()
{
	return reader->Get("config", "sip_pass", "UNKNOWN");
}

string configFile::sipCallcenterNum()
{
	return reader->Get("config", "sip_call_center", "UNKNOWN");
}

string configFile::gsmCallcenter_1()
{
	return reader->Get("config", "gsm_call_center_1", "UNKNOWN");
}

string configFile::gsmCallcenter_2()
{
	return reader->Get("config", "gsm_call_center_2", "UNKNOWN");
}

string configFile::MicVolumeLevel()
{
        return reader->Get("config", "Microphone", "UNKNOWN");
}

string configFile::SpeakerVolumeLevel()
{
        return reader->Get("config", "Speaker", "UNKNOWN");
}

