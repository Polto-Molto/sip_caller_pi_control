#ifndef GSMCONTROLLER
#define GSMCONTROLLER

#include "defines.h"
#include "pisetup.h"
#include "logfile.h"
#include "gsmusersfile.h"
#include "configfile.h"

#define KEYS 12

#define KEY_0 0
#define KEY_1 1
#define KEY_2 2
#define KEY_3 3
#define KEY_4 4
#define KEY_5 5
#define KEY_6 6
#define KEY_7 7
#define KEY_8 8
#define KEY_9 9
#define KEY_STAR 10
#define KEY_BLOCK 11

class gsmController
{
public:
enum STATUS {
    DIALING,
    RING,
    CONNECTED,
    NOT_CONNECTED,
    NO_CARRIER,
    BUSY,
    NO_DIAL_TONE,
    READY,
    ERROR
};
public:
    gsmController(string,int,configFile*);
    void connectToPort();
    bool connectionStatus();

    void setPiLink(piSetup*);
    void setAllowedNumbersFile(gsmUsersFile*);
    void setLoggerFile(logFile*);
    void setMicVolumeLevel(string);
    void setSpeakerVolumeLevel(string);
    void reset();

    void call(string);
    void hangUpCall();
    void answerCall();
    void startRegisteration();
    void setFunction();
    void setBaudrate(string);
    void activateDTMF();

    bool DTMFKey_status(int);
    int  micLevel();
    int  signal();
    bool registeration();
    bool callEnabled();
    
    void request_Registeration();
    void request_DetectDTMF();
    void request_ModuleFunction();
    void request_CallEnable();
    void request_SIMEnable();
    void request_Signal();
    void request_CallLineID();
    void request_MuteControl();
    void request_MicLevel();
    void request_ListCurrentControl();
    void request_DTMFToneGen();
    void request_DTMFToneDuration();
    void request_MobileOriginatedCall();
    void request_ModuleBaudRate();

private:

    bool manipulateData(string);
    void response_Registeration(string);

    void response_DetectDTMF(string);
    void response_ModuleFunction(string);
    void response_CallEnable(string);
    void response_SIMEnable(string);
    void response_Signal(string);
    void response_CallLineID(string);
    void response_MuteControl(string);
    void response_MicLevel(string);
    void response_ListCurrentControl(string);
    void response_DTMFToneGen(string);
    void response_DTMFToneDuration(string);
    void response_MobileOriginatedCall(string);
    void response_ModuleBaudRate(string);

    void response_Incoming_Call(string);

    void send_thread();
    void receive_thread();

    vector<string> explode(string, char);

private:
    bool first_init;
    bool connection_status;
    piSetup* _pi;
    gsmUsersFile *gsm_users;
    logFile *logfile;

    int device_handle;
    thread reading;
    thread writing;
    bool module_started;
    string _device;
    int  _baudrate;
    bool _registeration;
    bool full_function;
    bool call_enabled;
    bool sim_inside;
    bool incoming_call;
    string netwrok_name;
    int _signal;
    char last_dtmf;
    int mic_level;
    bool _key[KEYS];
    int register_failed_count;
    string mic_volume;
    string speaker_volume;

   configFile *conf;
};

#endif
