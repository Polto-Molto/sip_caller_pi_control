#ifndef DEFINES
#define DEFINES

#include "defines.h"
#include "mycall.h"
#include "pisetup.h"

class MyAccount : public Account
{
public:
    MyAccount();
    ~MyAccount();
    virtual void onRegStarted(OnRegStartedParam &prm);
    virtual void onRegState(OnRegStateParam &prm);
    virtual void onIncomingCall(OnIncomingCallParam &iprm);

    void setPiSetup(piSetup *);
    bool callEnabled();
private:
    void setCallEnabled(bool);

private:
    bool call_enabled;
   piSetup *_pi;
};
#endif
