#ifndef GSM_USERS_FILE
#define GSM_USERS_FILE
#include "defines.h"

class gsmUsersFile
{
public:
	gsmUsersFile(string);
	int openFile();
	bool loadData();
	string isUserInList(string);
	void printUsers();

private:
	string _name;
	fstream _file;
	map<string,string> _users;
};

#endif
