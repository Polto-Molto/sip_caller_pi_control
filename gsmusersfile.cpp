#include "gsmusersfile.h"
const char * CSV_USERS_FILE_DELIMS = ";";

gsmUsersFile::gsmUsersFile(string n)
	:_name(n)
{
    _file.open(n); 
}

int gsmUsersFile::openFile()
{
	return 0;
}

bool gsmUsersFile::loadData()
{
	bool state = false;
	char buffer[GSM_BASE_MAX_LINE_LENGTH] = {};
  while ( _file.getline(buffer, GSM_BASE_MAX_LINE_LENGTH) ) {
	 if(strlen(buffer) > 1)
        {
        const char * first = strtok( buffer, CSV_USERS_FILE_DELIMS);
        const char * second = strtok( NULL, CSV_USERS_FILE_DELIMS);
        string name(first);
        string num(second);
	//num.erase(num.length()-1,1);
        if(name.size() > 0 && num.size() > 0 )
        	_users[string(name)]= string(num);
	}  
    }
  if(_users.size() > 0 )
  	state = true;
  else
  	state = false;

	return state;
}

void gsmUsersFile::printUsers()
{
          for( const auto  &i  : _users)
          {
            cout << i.first << "   :::---" << i.second  << "---" << endl;
          }

}

string gsmUsersFile::isUserInList(string num)
{
	string user = "";
	  for( const auto  &i  : _users)
	  {
	  	if(num == i.second )
	  	{
	  		user = i.first;
			break;
	  	}
	  }
	return user;
}

