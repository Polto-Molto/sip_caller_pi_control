#include "defines.h"
#include "sipcall.h"
#include "pisetup.h"
#include "gsmcontroller.h"
#include "configfile.h"
#include "logfile.h"
#include "gsmusersfile.h"

extern int call_status = IDLE;
bool *connection_flag = new bool(true);
bool gsm_call_enable = false;
string sip_host;

string exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) throw std::runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
            result += buffer.data();
    }
    return result;
}

void testConnection(piSetup *pi,string host)
{
   while(1)
   {
     pi->setHeartUp();
     delay(500);
     pi->setHeartDown();
     delay(500);
     if(pi->isSwitchToGsm())
     {
        call_status = GSM_CALL;
        pi->switchToGsm(false);
     }
  }
}

void make_gsm_call(gsmController* gsm ,string number1, string number2,piSetup* pi)
{
   cout << "starting gsm call ....... " << endl;
   pi->setPowerSwitchActivation(false);
   gsm->call(number1);
}


void make_sip_call(sipCall *calling, string callnum ,piSetup* pi)
{
//    sipCall *calling = new sipCall(pi,host,name,pass,connection_flag);
//    calling->setLogfile(f);

    if(calling->callEnabled())
    {
      cout << "starting sip call ....... " << endl;
      pi->setIPCallOn();
      calling->startCall(callnum);
    }
    else
    {
      cout << "cannot start sip call ..." << endl;
      pi->setIPCallOff();
      pi->switchToGsm(true);
      //   call_status = GSM_CALL;
/*         if(calling != 0)
         {
          delete calling;
          calling = 0;
        }*/
    }
}

void catch_switch(piSetup* pi)
{
  while(1)
  {
    if( digitalRead (POWER_SWITCH) > 0 &&  pi->powerSwitchActivation() )
    {
      string cmd = "/home/pi/sip_caller_pi_control/check_sip_server.sh " + sip_host;
      string out = exec(cmd.c_str());
      cout << "checking sip caller server : "<< sip_host << " status : " <<out<<endl;
      out.erase(out.size() - 1);
      if(out[0] == 'y')
      {
      pi->setPowerSwitchPushed(true);
      pi->setPowerSwitchActivation(false);
      string cmd = string("aplay ")+WAV_PTAH+VOICE_WAIT_RESPONSE;
      system(cmd.data());
      delay(1000);
      call_status = SIP_CALL;
     }
     else
     {
      cout << "cannot start sip call ..." << endl;
      pi->setIPCallOff();
      pi->switchToGsm(true);
     }
    }
    else
      pi->setPowerSwitchPushed(false);
    delay(100);
  }
}

int main(int argc , char* argv[])
{

  configFile *conf = new configFile(CONFIG_PTAH+FILE_MAIN_INFO);
  gsmUsersFile *gsm_users = new gsmUsersFile(CONFIG_PTAH+FILE_GSM_BASE);
  logFile *logfile = new logFile(CONFIG_PTAH+FILE_GSM_LOG);
//  logFile *ip_logfile = new logFile(CONFIG_PTAH+FILE_IP_LOG);
  gsm_users->loadData();
  gsm_users->printUsers();

  string host = conf->sipHost();
  sip_host = conf->sipHost();
  string name = conf->sipUser();
  string pass = conf->sipPass();
  string sip_num = conf->sipCallcenterNum();
  string gsm_num_1 = conf->gsmCallcenter_1();
  string gsm_num_2 = conf->gsmCallcenter_2();
  string mic_volume =  conf->MicVolumeLevel();
  string speaker_volume =  conf->SpeakerVolumeLevel();

  string ping_check("ping -c 1 ");
  ping_check += host;
  if(system(ping_check.data()) == 0)
  {
    string cmd = string("aplay ")+WAV_PTAH+VOICE_INTERNET_PRESENT;
    system(cmd.data());
    cout << "sip server ping is ok " << endl;
  }
  else
  {
    string cmd = string("aplay ")+WAV_PTAH+VOICE_NO_INTERNET;
    system(cmd.data());
    cout << " not ping" << endl;
  }
  piSetup *pi = new piSetup;
  pi->init();
  pi->switchGsmModuleOn();


  string cmd = string("aplay ")+WAV_PTAH+VOICE_OS_LOADED;
  system(cmd.data());
  delay(3000);
  gsmController *gsm = new gsmController("/dev/ttyAMA0" , 115200,conf);
  
  gsm->setBaudrate("115200");
  gsm->setPiLink(pi);
  gsm->setAllowedNumbersFile(gsm_users);
  gsm->setLoggerFile(logfile);
  gsm->setMicVolumeLevel(mic_volume);
  gsm->setSpeakerVolumeLevel(speaker_volume);
  cout << " start gsm registration" << endl;
  gsm->startRegisteration();
  gsm->activateDTMF();
  
  if(gsm->connectionStatus())
	gsm_call_enable = true;
  else
  {
    string cmd = string("aplay ")+WAV_PTAH+VOICE_NO_GSM;
    system(cmd.data());
  }

  std::thread connection(testConnection,pi,host);
  std::thread switch_monitor (catch_switch,pi);
  std::thread *call_thread = 0;
  bool *connection_flag = new bool(true);

  sipCall *calling = new sipCall(pi,host,name,pass,connection_flag);

  while(1)
  {
    switch(call_status)
    {
      case SIP_CALL:
	        if(call_thread != 0)
        	{
                   call_thread->join();
                   delete call_thread;
	           call_thread=0;
        	}
                call_status = IDLE;

  //              call_thread = new std::thread(make_sip_call,host,name,pass,sip_num,pi,ip_logfile);
		make_sip_call(calling,sip_num,pi);
        	break;

      case GSM_CALL:
	       if(gsm->connectionStatus())
        	gsm_call_enable = true;
                if(call_thread != 0)
                {
                   call_thread->join();
                   delete call_thread;
		  call_thread=0;
                }
                call_status = IDLE;
                call_thread = new std::thread(make_gsm_call,gsm,gsm_num_1,gsm_num_2,pi);
      	        break;

      default:
              call_status = IDLE;
	      break;
      }
    delay(100);
  }

  return 0;
}
