#ifndef CONFIG_FILE
#define CONFIG_FILE
#include "defines.h"
#include "INIReader.h"

class configFile
{
public:
	configFile(string);
	int openFile();
	int loadData();

	string sipHost();
	string sipUser();
	string sipPass();
	string sipCallcenterNum();
	string gsmCallcenter_1();
	string gsmCallcenter_2();
	string MicVolumeLevel();
	string SpeakerVolumeLevel();

private:
	string _name;
  	INIReader *reader;
};

#endif
