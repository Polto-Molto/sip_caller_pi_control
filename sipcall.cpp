#include "sipcall.h"

sipCall::sipCall(piSetup* pi,string h,string n,string p,bool* con)
			:_pi(pi),_host(h),_name(n),_pass(p)
{
    connection_flag = con;
    acc = 0;
    call = 0;
    ep.libCreate();

    // Initialize endpoint
    ep_cfg.medConfig.hasIoqueue = false;
    ep.libInit( ep_cfg );

    // Create SIP transport. Error handling sample is shown
    tcfg.port = 0;
    try {
        ep.transportCreate(PJSIP_TRANSPORT_TCP, tcfg);
        ep.transportCreate(PJSIP_TRANSPORT_UDP, tcfg);
    } catch (Error &err) {
        std::cout << err.info() << std::endl;
    }
        ep.libStart();
        pj_thread_sleep(2000);
    account_register();
}

sipCall::~sipCall()
{
// delete call;
/*
 ep.hangupAllCalls();
 if(acc != 0)   
    delete acc;
 ep.libDestroy();
*/
}


void sipCall::account_register()
{
    AccountConfig acc_cfg;
    string uri = "sip:"+_name+"@"+_host;
    acc_cfg.idUri = uri;
    string reg = "sip:"+_host;
    acc_cfg.regConfig.registrarUri = reg;
    acc_cfg.sipConfig.authCreds.push_back( AuthCredInfo("digest", "*",_name, 0, _pass) );
    acc_cfg.callConfig.timerMinSESec = 90;
    acc_cfg.callConfig.timerSessExpiresSec = 120;
    acc_cfg.callConfig.timerUse =PJSUA_SIP_TIMER_ALWAYS;
    acc = new MyAccount;
    try {
        acc->create(acc_cfg);
        acc->setPiSetup(_pi);
    } catch(Error& err) {
               pj_thread_sleep(2000);
	account_register();
        cout << "Account creation error: " << err.info() << endl;
    }
        pj_thread_sleep(2000);
}


void sipCall::startCall(string c)
{
    int duration = 0;
    _caller = c;
    call = new MyCall(*acc);
    call->setPiSetup(_pi);
    CallOpParam prm(true); // Use default call settings
    try {
      string call_str = "sip:"+_caller+"@"+_host;
        call->makeCall(call_str, prm);
        _pi->switchToGsm(false);
/*      while(1)
	        {
          if(!call->isActive())
          {
                hangup();
                break;
          }
          pj_thread_sleep(1000);
        }
*/
    } catch(Error& err) {
        cout << err.info() << endl;
	hangup();
        _pi->switchToGsm(true);
    }
    pj_thread_sleep(3000);

/*
    if(!((MyCall*)call)->isCallAnswered())
        _pi->switchToGsm(true);
    else
        _pi->switchToGsm(false);


        while(1)
        {
	  if(!call->isActive())
	  {
		hangup();
		break;
	  }
//          pj_thread_sleep(1000);
        }
/*        _pi->setIPCallOff();
        _pi->setPowerSwitchActivation(true);
        _pi->setSipCallAnswered(false);
*/
//        if(this != 0)
//    	delete this;
}

void sipCall::hangup()
{
   ep.hangupAllCalls();
}

bool sipCall::callEnabled()
{
    return acc->callEnabled();
}

